#include <iostream>
#include <array>

using namespace std;

int main()
{
    cout << "Hello World!" << endl;
    array<double, 4> arr {};
    for (const auto& el : arr)
        cout << el << ", ";
    cout << endl;
    cout << get<1>(arr) << endl;
    return 0;
}

