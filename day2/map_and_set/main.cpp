#include <iostream>
#include <set>
#include <functional>
#include <map>
#include <memory>

using namespace std;

int main()
{
    multiset<int, greater<int>> s;
    s.insert(4);
    s.insert(2);
    s.insert(5);
    s.insert(5);
    //if (s.insert(5).second)
    //    cout << "is inserted" << endl;

    for (auto& el : s)
        cout << el << ", ";
    cout << endl;



//    set<unique_ptr<int>, std::function<bool(const unique_ptr<int>&, const unique_ptr<int>&)>> sup
//                    ([](const unique_ptr<int>& a, const unique_ptr<int>& b) {return *a < *b;});

    auto cmp = [](const unique_ptr<int>& a, const unique_ptr<int>& b) {return *a < *b;};
    set<unique_ptr<int>, decltype(cmp)> sup(cmp);
    sup.insert(make_unique<int>(4));
    sup.insert(make_unique<int>(5));
    sup.insert(make_unique<int>(2));

    for (auto& el : sup)
        cout << *el << ", ";
    cout << endl;

    map<string, double> stocks;
    stocks.insert(pair<string, double>("NKIA", 12.4));
    stocks.insert(make_pair("GOGL", 13.4));

    stocks["IBM"] = 13.45;

    cout << stocks["AAA"] << endl;

    for (const auto& el : stocks)
        cout << el.first << " : " << el.second << endl;

    return 0;
}

