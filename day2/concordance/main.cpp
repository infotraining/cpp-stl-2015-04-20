#include <iostream>
#include <map>
#include <unordered_map>
#include <fstream>
#include <string>
#include <boost/algorithm/string.hpp>

using namespace std;

int main()
{
    cout << "Swanns Way" << endl;
    ifstream inp("swann.txt");
    if (!inp) cerr << "error opening file" << endl;

    unordered_map<string, int> freq;
    string word;
    while( inp >> word)
    {
        boost::to_lower(word);
        boost::trim_if(word, boost::is_any_of("\t!:;()'-\"-.,?"));
        freq[move(word)]++;
    }

    map<int, vector<string>, greater<int>> result;

    for (const auto& el : freq)
    {
        //cout << el.first << " : " << el.second << endl;
        result[el.second].push_back(el.first);
    }

    for(const auto& el : result)
    {
        cout << el.first << " : ";
        for (const auto& word : el.second)
            cout << word << ", ";
        cout << endl;
    }



    return 0;
}

