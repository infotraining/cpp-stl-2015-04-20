#include <iostream>
#include <vector>
#include <deque>
#include <list>
#include <ctime>
#include <algorithm>
#include <cstring>
#include <chrono>
#include <random>

using namespace std;

class ExpensiveObject
{
private:
    char* data_;
    size_t size_;
public:
    ExpensiveObject(const char* data) : data_(NULL), size_(std::strlen(data))
    {
        data_ = new char[size_+1];
        std::strcpy(data_, data);
    }

    ExpensiveObject(const ExpensiveObject& source) : data_(NULL), size_(source.size_)
    {
        data_ = new char[size_+1];
        std::strcpy(data_, source.data_);
    }

    // move ctor
    ExpensiveObject(ExpensiveObject&& source) noexcept : data_(source.data_), size_(source.size_)
    {
        source.data_ = nullptr;
        source.size_ = 0;
    }

    ~ExpensiveObject()
    {
        delete [] data_;
    }

    ExpensiveObject& operator=(const ExpensiveObject& source)
    {
        ExpensiveObject temp(source);
        swap(temp);
        return *this;
    }

    ExpensiveObject& operator=(ExpensiveObject&& source) noexcept
    {
        if( &source != this)
        {
            delete [] data_;
            data_ = source.data_;
            size_ = source.size_;

            source.data_ = nullptr;
            source.size_ = 0;
        }
        return *this;
    }

    void swap(ExpensiveObject& other)
    {
        std::swap(data_, other.data_);
        std::swap(size_, other.size_);
    }

    const char* data() const
    {
        return data_;
    }

    size_t size() const
    {
        return size_;
    }

    bool operator<(const ExpensiveObject& other) const
    {
        if (std::strcmp(data_, other.data_) < 0)
            return true;
        return false;
    }
};

ExpensiveObject expensive_object_generator()
{
    static char txt[] = "abcdefghijklmn";
    static const size_t size = std::strlen(txt);

    std::random_shuffle(txt, txt + size);

    return ExpensiveObject(txt);
}

// szablony funkcji umożliwiające sortowanie kontenera

template<typename Cont>
void sort(Cont& cont)
{
    sort(std::begin(cont), std::end(cont));
}

template<typename T>
void sort(list<T>& cont)
{
    cont.sort();
}

template<typename Cont, typename Gen>
void test(Cont& v, Gen g,const int SIZE)
{
    auto start = chrono::high_resolution_clock::now();

    for(int i = 0 ; i < SIZE ; ++i)
    {
        v.push_back(g());
    }
    auto end_creat = chrono::high_resolution_clock::now();

    sort(v);

    auto end_sort = chrono::high_resolution_clock::now();

    cout << "Elapsed - creating = ";
    cout << chrono::duration_cast<chrono::microseconds>(end_creat-start).count();
    cout << " us" << endl;
    cout << "Elapsed - sorting = ";
    cout << chrono::duration_cast<chrono::microseconds>(end_sort-end_creat).count();
    cout << " us" << endl;
}



int main()
{

/*
    Utwórz trzy sekwencje typu int: vector, deque i list. Wypelnij je wartosciami losowymi.
    Porównaj czasy tworzenia i wypelniania danymi sekwencji. Napisz szablon funkcji sortującej sekwencje
    vector i deque. Napisz specjalizowany szablon funkcji realizującej sortowanie dla kontenera list.
    Porównaj efektywność operacji sortowania.
*/

    const int SIZE = 1000000;
    random_device rd;
    mt19937_64 mt(1);
    uniform_int_distribution<> dist(0, 1000);

    cout << "vector" << endl;
    vector<int> v;
    //v.reserve(SIZE);
    test(v, [mt, dist] () mutable {return dist(mt);}, SIZE);

    cout << "deque" << endl;
    deque<int> d;
    test(d, [mt, dist] () mutable {return dist(mt);}, SIZE);

    cout << "list" << endl;
    list<int> l;
    test(l, [mt, dist] () mutable {return dist(mt);}, SIZE);

    cout << "vector - expensive" << endl;
    vector<ExpensiveObject> ve;
    ve.reserve(SIZE);
    test(ve, expensive_object_generator, SIZE);

    cout << "deque - expensive" << endl;
    deque<ExpensiveObject> de;
    test(de, expensive_object_generator, SIZE);

    cout << "list - expensive" << endl;
    list<ExpensiveObject> le;
    test(le, expensive_object_generator, SIZE);


}
