#include <vector>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <functional>
#include <cmath>
#include <numeric>

using namespace std;

class RandGen
{
    int range_;
public:
    RandGen(int range) : range_(range)
    {
    }

    int operator()() const
    {
        return rand() % range_;
    }
};

struct Avg
{
    long sum{};
    double avg{};
    long min{};
    long max{};
    long i{};
    void operator()(int x)
    {
        ++i;
        sum += x;
        if (x < min) min = x;
        if (x > max) max = x;
        avg = double(sum)/i;
    }
};

template <typename Cont>
void print(Cont c)
{
    cout << "[ ";
    for (const auto& el : c)
        cout << el << ", ";
    cout << " ]" << endl;
}

int main()
{
    vector<int> vec(25);

    //int a = 0;
    auto f = + []() { cout << "lambda "  << endl; };
    cout << typeid(f).name() << endl;

    generate(vec.begin(), vec.end(), RandGen(30));

    print(vec);
    // 1a - wyświetl parzyste
    copy_if(vec.begin(), vec.end(), ostream_iterator<int>(cout, ", "),
            [] (int a) { return !(a % 2);});
    cout << endl;

    // 1b - wyswietl ile jest nieparzystych
    cout << "odd = " << count_if(vec.begin(), vec.end(),
                                 [] (int a) { return a % 2;}) << endl;

    // 2 - usuń liczby podzielne przez dowolną liczbę z tablicy eliminators
    int eliminators[] = { 3, 5, 7 };

    auto elim = [&eliminators] (int x) {return any_of(begin(eliminators),
                                                      end(eliminators),
                                   [x](int y){ return x%y == 0;});};

    auto garbage = remove_if(vec.begin(), vec.end(), elim);
    vec.erase(garbage, vec.end());
    print(vec);

    // 3 - tranformacja: podnieś liczby do kwadratu

    // 4 - wypisz 5 najwiekszych liczb
    nth_element(vec.begin(), vec.begin()+5, vec.end(), greater<int>());
    //sort(vec.begin(), vec.end(), greater<int>());
    copy_n(vec.begin(), 5, ostream_iterator<int>(cout, ", "));
    cout << endl;

    // 5 - policz wartosc srednia
    long sum{};
    for_each(vec.begin(), vec.end(), [&sum](int x) { sum += x;});
    cout << "Avg = " << double(sum)/vec.size() << endl;
    Avg avg;
    avg = for_each(vec.begin(), vec.end(), avg);
    cout << avg.avg << endl;
    cout << avg.min << endl;
    cout << avg.max << endl;

    // 6 - utwórz dwa kontenery - 1. z liczbami mniejszymi lub równymi średniej,
    //        2. z liczbami większymi od średniej
    vector<int> less_than_avg;
    vector<int> greater_than_avg;
    partition_copy(vec.begin(), vec.end(),
                   back_inserter(greater_than_avg),
                   back_inserter(less_than_avg),
                   [&avg](int x) { return x > avg.avg;});
    print(less_than_avg);
    print(greater_than_avg);

}
