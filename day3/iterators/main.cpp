#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <list>

using namespace std;

int main()
{
    vector<int> v1{1,2,3,4};
    vector<int> v2;

    for(auto it = v1.rbegin() ; it != v1.rend() ; ++it)
        cout << *it << endl;

    back_insert_iterator<vector<int>> bii(v2);

    copy(v1.begin(), v1.end(), bii);

    bii = 100;
    bii++;
    bii = 200;


    for(auto& el : v2)
        cout << el << endl;

//    istream_iterator<int> start(cin);
//    istream_iterator<int> end;

    //copy(start, end, bii);

    copy(v2.begin(), v2.end(), ostream_iterator<int>(cout, " "));
    cout << endl;

    //cout << accumulate(v1.begin(), v2.end(), 0) << endl;

    list<string> s{"one", "two", "three"};

    //vector<string> vs(make_move_iterator(s.begin()), make_move_iterator(s.end()));

    vector<string> vs;
    move(s.begin(), s.end(), back_inserter(vs));

    cout << "vector" << endl;
    for(const auto& s : vs)
        cout << s << ", ";
    cout << endl;

    cout << "list" << endl;
    for(const auto& w : s)
        cout << w << ", ";
    cout << endl;

    return 0;
}

