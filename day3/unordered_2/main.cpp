#include <iostream>
#include <unordered_set>
#include <random>
#include <chrono>
#include <boost/functional/hash.hpp>

using namespace std;

struct Point
{
    int x, y;
    Point(int x, int y) : x(x), y(y) {}

    bool operator==(const Point& p2) const
    {
        return (x == p2.x) && (y == p2.y);
    }
};

namespace std
{
    template<>
    struct hash<Point>
    {
         size_t operator()(const Point& p) const
         {
//             return p.x;
             size_t seed = 0;
             boost::hash_combine(seed, p.x);
             boost::hash_combine(seed, p.y);
             //return (p.x+501)*(p.y+501);
             return seed;
         }
    };
}

int main()
{
    cout << "Point in set" << endl;
    unordered_set<Point> pts;
    random_device rd;
    mt19937_64 gen(rd());
    uniform_int_distribution<> dist(-500, 500);

    auto start = chrono::high_resolution_clock::now();
    pts.rehash(100000);
    for (int i = 0 ; i < 100000 ; ++i)
        pts.emplace(dist(gen), dist(gen));

    auto end_insert = chrono::high_resolution_clock::now();

    cout << "Elapsed - creating = ";
    cout << chrono::duration_cast<chrono::milliseconds>(end_insert-start).count();
    cout << " ms" << endl;

    cout << "statistics:" << endl;
    cout << "Size = " << pts.size() << endl;
    cout << "Load factor = " << pts.load_factor() << endl;
    cout << "Bucket count = " << pts.bucket_count() << endl;

    size_t max_counts = 0;
    size_t bucket_more_than_ten = 0;
    for(int i = 0 ; i < pts.bucket_count() ; ++i)
    {
        if (pts.bucket_size(i) > max_counts)
            max_counts = pts.bucket_size(i);
        if (pts.bucket_size(i) > 10) bucket_more_than_ten++;
    }
    cout << "Max size of bucket = " << max_counts << endl;
    cout << "Bucket with > 10 = " << bucket_more_than_ten << endl;

    auto start_search = chrono::high_resolution_clock::now();

    long hits{};
    for(int i = 0 ; i < 100000 ; ++i)
    {
        if(pts.count(Point(dist(gen), dist(gen))))
            hits++;
    }
    cout << "Hits = " << hits << endl;

    auto end_search = chrono::high_resolution_clock::now();

    cout << "Elapsed - searching = ";
    cout << chrono::duration_cast<chrono::milliseconds>(end_search-start_search).count();
    cout << " ms" << endl;

    return 0;
}

