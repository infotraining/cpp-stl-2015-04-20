#include <iostream>
#include <vector>
#include <deque>

using namespace std;

class Loud
{
    int id;
public:
    Loud(int id) : id(id)
    {
        cout << "ctor " << id << endl;
    }

    Loud(const Loud& other)
    {
        id = other.id;
        cout << "copy ctor " << id << endl;
    }

    Loud& operator=(const Loud& other)
    {
        id = other.id;
        cout << "copy = " << id << endl;
    }

    Loud(Loud&& other) noexcept
    {
        id = other.id;
        other.id = 0;
        cout << "move ctor " << id << endl;
    }

    Loud& operator=(Loud&& other) noexcept
    {
        id = other.id;
        cout << "move = " << id << endl;
    }

    ~Loud()
    {
        cout << "dtor " << id << endl;
    }
};


int main()
{
    {
        vector<Loud> vl;
        //vl.reserve(10);
        vl.push_back(Loud(1));
        vl.emplace_back(2);
        cout << "*******" << endl;
        vl.emplace_back(3);
        cout << "*******" << endl;
    }
    cout << "------ deque -------" << endl;
    {
        deque<Loud> vl;
        //vl.reserve(10);
        vl.emplace_back(1);
        cout << "*******" << endl;
        vl.emplace_back(2);
        cout << "*******" << endl;
    }
    return 0;
}

