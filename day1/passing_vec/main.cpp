#include <iostream>
#include <vector>
#include <memory>

using namespace std;

void print(vector<int> vec)
{
    for(auto& el : vec)
        cout << el << ", ";
    cout << endl;

}

void print_c(const vector<int>& vec)
{
    for(auto& el : vec)
        cout << el << ", ";
    cout << endl;
}

void print_s(std::unique_ptr<vector<int>> ptr)
{
    for(auto& el : *ptr)
        cout << el << ", ";
    cout << endl;
}

int main()
{
    vector<int> vec{1,2,3,4,5};
    //print_s(make_unique<vector<int>>(10, -1));
    print(move(vec));
    return 0;
}

