#include <iostream>
#include <vector>
#include <string>

using namespace std;

int main()
{
    cout << "Hello vector!" << endl;

    int a[5];
    a[1] = 100;
    cout << a[1] << endl;
    cout << *(a+1) << endl;
    cout << 1[a] << endl;

    vector<int> v{1,2,3,4,5};
    std::initializer_list<int> inl = {1,2,3,4,5};
    for (auto el : inl)
    {
        cout << el << endl;
    }

    for(int i = 0 ; i < v.size() ; ++i)
        cout << v[i] << ", ";

    cout << endl;

    for(vector<int>::const_iterator it = v.begin() ;
        it != v.end() ; ++it)
    {
        cout << *it << ", ";
    }

    cout << endl;

    for(auto it = v.cbegin() ; it != v.cend() ; ++it)
    {
        cout << *it << ", ";
    }

    cout << endl;

    for(auto it = begin(v) ; it != end(v) ; ++it)
    {
        const auto& el = *it;
        cout << el << ", ";
    }

    cout << endl;

    for(const auto& el : v)
        cout << el << ", ";
    cout << endl;

    vector<int> v2{};
    v2.reserve(100);
    v2.resize(100);
    v2.shrink_to_fit();


    for(int i = 0 ; i < 100 ; ++i)
    {
        v2.push_back(1);
        cout << "size: " << v2.size();
        cout << " cap: " << v2.capacity() << endl;
    }

    vector<string> vs;
    vs.push_back(string("ala ma kota"));
    vs.emplace_back("a kot ma ale");

    cout << endl;

    return 0;
}

