#include "person.hpp"

#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <functional>

using namespace std;

auto check_salary(double salary)
{
    return [salary](const auto& p)
    {
         return p.salary() > salary;
    };
}

int main()
{
    vector<Person> employees;
    fill_person_container(employees);

    cout << "Wszyscy pracownicy:\n";
    copy(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"));
    cout << endl;

    // wyświetl pracownikow z pensją powyżej 3000
    cout << "salary over 3000" << endl;

    copy_if(employees.begin(), employees.end(),
            ostream_iterator<Person>(cout,"\n"),
            check_salary(3000));

    for(const auto& p : employees)
        if (p.salary() > 3000)
            cout << p << endl;


    // wyświetl pracowników o wieku poniżej 30 lat

    // posortuj malejąco pracownikow wg nazwiska
    cout << "sorted by name" << endl;
    sort(employees.begin(), employees.end(),
         [] (const Person& p1, const Person& p2) { return p1.name() < p2.name(); });

    copy(employees.begin(), employees.end() ,ostream_iterator<Person>(cout,"\n"));

    // wyświetl kobiety

    // ilość osob zarabiajacych powyżej średniej
}
