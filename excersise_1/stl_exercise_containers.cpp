#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <cassert>
#include <list>
#include <functional>
#include <iterator>

using namespace std;

template<typename A, typename B>
auto add(A a, B b) -> decltype(a+b)
{
    int z = a+b;
    return z;
}

template <typename Cont>
void print(Cont c)
{
    cout << "[ ";
    for (const auto& el : c)
        cout << el << ", ";
    cout << " ]" << endl;
}

void avg(int array[], size_t size)
{
    double sum = 0.0;
    for(size_t i = 0; i < size; ++i)
        sum += array[i];

    cout << "AVG: " << sum / size << endl;
}

bool is_even(int n)
{
    return n % 2 == 0;
}

auto main() -> int
{
    int tab[100];

    for(int i = 0; i < 100; ++i)
        tab[i] = rand() % 100;

    cout << "tab: ";
    for(int i = 0; i < 100; ++i)
        cout << tab[i] << " ";
    cout << "\n\n";

    // 1 - utwórz wektor vec_int zawierający kopie wszystkich elementów z  tablicy tab
    //vector<int> vec_int(tab, tab+100); // c++98
    vector<int> vec_int(begin(tab), end(tab)); // c++11 and up only
    print(vec_int);

    // 2 - wypisz wartość średnią przechowywanych liczb w vec_int
    //avg(&vec_int[0],100);
    avg(vec_int.data(), vec_int.size());

    // 3a - utwórz kopię wektora vec_int o nazwie vec_cloned
    vector<int> vec_cloned = vec_int;

    // 3b - wyczysć kontener vec_int i zwolnij pamięć po buforze wektora
    vec_int.clear();
    //vector<int>().swap(vec_int);
    vec_int.shrink_to_fit(); // C++11
    cout << "capacity after clear = " << vec_int.capacity() << endl;

    // 4 - dodaj na koniec wektora vec_cloned zawartość tablicy rest
    int rest[] = {1,2,3,4};
    vec_cloned.insert(vec_cloned.end(), rest, rest+4);
    print(vec_cloned);

//    for (auto& el : rest)
//    {
//        vec_cloned.push_back(el);
//    }

    // 5 - posortuj zawartość wektora vec_cloned
    sort(begin(vec_cloned), end(vec_cloned));
    //sort(vec_cloned.begin(), vec_cloned.end());
    print(vec_cloned);

    // 6 - wyświetl na ekranie zawartość vec_cloned za pomocą iteratorów
    //for(vector<int>::const_iterator it = vec_cloned.begin() ;...)

    // 7 - utwórz kontener numbers (wybierz odpowiedni typ biorąc pod uwagę następne polecenia) zawierający kopie elementów vec_cloned

    // 8 - usuń duplikaty elementów przechowywanych w kontenerze

    // 9 - usuń liczby parzyste z kontenera

    // 10 - wyświetl elementy kontenera w odwrotnej kolejności

    // 11 - skopiuj dane z numbers do listy lst_numbers jednocześnie i wyczyść kontener numbers

    // 12 - wyświetl elementy kontenera lst_numbers w odwrotnej kolejności
}

