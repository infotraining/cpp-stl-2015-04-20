#include <iostream>
#include <fstream>
#include <set>
#include <unordered_set>
#include <string>
#include <chrono>
#include <iterator>
#include <algorithm>
#include <boost/algorithm/string.hpp>
#include <sstream>

using namespace std;

int main()
{
     // wczytaj zawartość pliku en.dict ("słownik języka angielskieog")
     // sprawdź poprawość pisowni następującego zdania:

    string input_text = "this is an exple of snetence";

    //unordered_set<string> dict;
    //set<string> dict;
    vector<string> dict;
    string word;

    ifstream in("/home/leszek/_code_/excersise_3/pl.dict");
    if (!in) cerr << "error opening file" << endl;

    dict.reserve(5000000);
//    dict.rehash(5000000);
//    cout << "Load factor " << dict.load_factor() << endl;
//    cout << "Max Load factor " << dict.max_load_factor() << endl;
//    cout << "Bucket Count " << dict.bucket_count() << endl;


    auto start = chrono::high_resolution_clock::now();

    // creating dict
    auto prev = dict.end();
    while(in >> word)
    {
        //prev = dict.emplace_hint(prev, move(word));
        dict.emplace_back(move(word));
    }
    sort(dict.begin(), dict.end());
        //dict.insert(word);

    auto end_creat = chrono::high_resolution_clock::now();

//    cout << "Load factor " << dict.load_factor() << endl;
//    cout << "Max Load factor " << dict.max_load_factor() << endl;
//    cout << "Bucket Count " << dict.bucket_count() << endl;

    // splitting
    vector<string> words;
    boost::split(words, input_text, boost::is_any_of(" "));

    // checking
    for(const auto& el : words)
    {
        //if (dict.count(el) == 0)
        if (!binary_search(dict.begin(), dict.end(), el))
            cout << el << ", ";
    }
    cout << endl;

    auto end_check = chrono::high_resolution_clock::now();

    cout << "Elapsed - creating = ";
    cout << chrono::duration_cast<chrono::milliseconds>(end_creat-start).count();
    cout << " ms" << endl;
    cout << "Elapsed - checking = ";
    cout << chrono::duration_cast<chrono::microseconds>(end_check-end_creat).count();
    cout << " us" << endl;

}

